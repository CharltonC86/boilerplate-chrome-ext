## Main Folder Structure
    demo/                       // this is the folder to be selected when "load unpacked extension"
        content/                // content for the content script (js, css)
        devtool/                // content for Devtool tab (html, js, css, img)
        option/                 // content Extension Option/Setting (html, js, css, img)
        popup/                  // content for Popup when Extension Icon is clicked (html, js, css, img)
        worker/                 // content for the Background/Worker Script (js)
        manifest.json           // entry point (config file) for chrome extension

## Getting Started
1. Open Chrome
2. Go to chrome://extensions/
3. Click "Load unpacked extension"
4. Select the folder, in this case the "demo" folder
5. Enable the Extension
